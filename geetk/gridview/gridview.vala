/* 
 * Medsphere.Widgets
 * Copyright (C) 2007 Medsphere Systems Corporation
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * Vala port 2008 by Frederik.
 *
 * Remarks specific to this Vala port are tagged with 'XXX-VALA' in order to 
 * distinguish them from the original remarks.
 */

using Gee;
using Gdk;
using Gtk;

namespace Geetk {

	public class GridSelection : GLib.Object {

		public GridView gv { private get; construct; }

		private TreePath _path = null;
		private GridViewColumn _col = null;

		public bool has_selection {
			get { return _path != null && _col != null; }
		}

		public signal void changed ();

		public GridSelection (GridView g) {
			this.gv = g;
		}

		construct {
			this.gv.columns_changed += on_grid_view_columns_changed;
		}

		public bool select_cell (TreePath path, GridViewColumn col) {
			if (!this.gv.get_selectable (path, col)) {
				return false;
			}

			if (path != null) {
				_path = path.copy ();
			} else {
				_path = null;
			}
			_col = col;

			changed ();

			return true;
		}

		public void deselect () {
			_path = null;
			_col = null;

			changed ();
		}

		public bool get_selected (out TreePath path, out GridViewColumn col) {
			if (_path != null) {
				path = _path.copy ();
			} else {
				path = null;
			}
			col = _col;

			return this.has_selection;
		}

		private void on_grid_view_columns_changed () {
			if (_path == null && _col == null) {
				return;
			}

			if (Array.index_of ((GLib.Object[]) this.gv.columns, _col) < 0) {
				deselect ();
			}
		}
	}

	public class GridViewColumn : GLib.Object {

		public signal void visibility_changed ();

		private bool _visible = true;

		private CellRenderer _header_renderer;
		private CellRenderer _field_renderer;
		private Gee.Map<string, int> _header_attrs = new Gee.HashMap<string, int> (str_hash, str_equal);
		private Gee.Map<string, int> _field_attrs = new Gee.HashMap<string, int> (str_hash, str_equal);

		private GridCellDataFunc _df = null;
		private GridCellDataFunc _hdf = null;

		public GridViewColumn (CellRenderer r /*, RenderAttribute[]? a */) {
			this.field_renderer = r;
		}

		// XXX-VALA: currently arrays can't get passed to construction methods,
		//           since array properties are not supported yet
		//           (Vala Bug #536706), so we provide a setter
		public void set_field_attrs (RenderAttribute[] a) {
			foreach (RenderAttribute attr in a) {
				_field_attrs.set (attr.property, attr.column);
			}
		}

		public bool visible {
			get { return _visible; }
			set {
				if (_visible == value) {
					return;
				}

				_visible = value;

				visibility_changed ();
			}
		}

		public CellRenderer field_renderer {
			get { return _field_renderer; }
			construct { _field_renderer = value; }
		}

		public CellRenderer header_renderer {
			get { return _header_renderer; }
		}

		// XXX-VALA: no delegate properties yet (Vala Bug #543879)
//		public GridCellDataFunc cell_data_func {
//			set { _df = value; }
//		}
		public void set_cell_data_func (GridCellDataFunc func) {
			_df = func;
		}

		// XXX-VALA: no delegate properties yet (Vala Bug #543879)
//		public GridCellDataFunc header_cell_data_func {
//			set { _hdf = value; }
//		}
		public void set_header_cell_data_func (GridCellDataFunc func) {
			_hdf = func;
		}

		public void set_header_renderer (CellRenderer r,
		                                 RenderAttribute[]? a = null)
		{
			_header_renderer = r;

			foreach (RenderAttribute attr in a) {
				_header_attrs.set (attr.property, attr.column);
			}
		}

		public void cell_set_cell_data (TreeModel m, TreeIter i, bool h) {
			Gee.Map<string, int> attrs;
			CellRenderer r;
			Value v = Value (typeof (int));	// XXX-VALA

			if (h && _header_renderer != null) {
				r = _header_renderer;
				attrs = _header_attrs;
			} else {
				r = _field_renderer;
				attrs = _field_attrs;
			}

			foreach (string prop in attrs.get_keys ()) {
				// XXX-VALA parameter v should be out instead of ref
				m.get_value (i, attrs.get (prop), ref v);
				r.set_property(prop, v);
			}

			if (h && _hdf != null) {
				_hdf (this, r, m, i);
			} else if (_df != null) {
				_df (this, r, m, i);
			}
		}
	}

	public delegate void GridCellDataFunc (GridViewColumn c, CellRenderer r,
	                                       TreeModel m, TreeIter i);

	public class GridView : Container {

		public enum ScrollbarSpan {
			HEADER_GAP,
			HEADER_OVERLAP,
			FULL
		}

		private class LinkedScrollbar : GLib.Object {

			public Scrollbar scrollbar { private get; construct; }

			public LinkedScrollbar (Scrollbar sb) {
				this.scrollbar = sb;
			}

			public void link (Widget widget) {
				widget.scroll_event += on_linked_widget_scroll_event;
			}

			public void link_all (Widget[] widgets) {
				foreach (Widget w in widgets) {
					link (w);
				}
			}

			/**
			 * the delta calculation is lifted from
			 * _gtk_range_get_wheel_delta (), which is unexposed c api
			 **/
			private bool on_linked_widget_scroll_event (Widget w,
			                                        Gdk.EventScroll event)
			{
				if (this.scrollbar is VScrollbar &&
				    (event.direction == ScrollDirection.LEFT ||
				     event.direction == ScrollDirection.RIGHT))
				{
					return false;
				} else if (this.scrollbar is HScrollbar &&
				           (event.direction == ScrollDirection.UP ||
				            event.direction == ScrollDirection.DOWN))
				{
					return false;
				}

				Adjustment adj = this.scrollbar.adjustment;
				double delta = Math.pow (adj.page_size, 2.0 / 3.0);

				if (event.direction == ScrollDirection.UP ||
				    event.direction == ScrollDirection.LEFT)
				{
					adj.value -= delta;
				} else {
					adj.value = double.min (adj.upper - adj.page_size,
					                        adj.value + delta);
				}

				return false;
			}
		}

		private static Cursor DRAG_CURSOR = new Cursor (CursorType.SB_H_DOUBLE_ARROW);

		public signal void columns_changed ();
		public signal void orientation_changed ();

		private Gee.List<Widget> _children = new Gee.ArrayList<Widget> ();

		private TreeModel _model;

		private Orientation _orientation;

		private Gee.List<GridViewColumn> _columns = new Gee.ArrayList<GridViewColumn> ();
		private Gee.List<GridViewColumn> _visible = new Gee.ArrayList<GridViewColumn> ();

		private Gee.List<int> _widths = new Gee.ArrayList<int> ();
		private Gee.List<int> _heights = new Gee.ArrayList<int> ();

		/* column header span, row header span */
		private int c_span = 1; /* c_span2 = boring; */
		private int r_span = 1;
		/* the column currently being resized, or -1 */
		private int _drag_col = -1;

		/**
		 * c = top left corner
		 * t = top
		 * l = left
		 * f = field
		 **/
		private Viewport tvp;
		private Viewport lvp;
		private Viewport fvp;
		private DrawingArea cda;
		private DrawingArea tda;
		private DrawingArea lda;
		private DrawingArea fda;

		private Scrollbar hbar;
		private Scrollbar vbar;
		/* XXX: is this what ScrolledWindow defaults to? */
		private PolicyType _hbar_policy = PolicyType.ALWAYS;
		private PolicyType _vbar_policy = PolicyType.ALWAYS;
		private ScrollbarSpan _hbar_span = ScrollbarSpan.HEADER_GAP;
		private ScrollbarSpan _vbar_span = ScrollbarSpan.HEADER_GAP;

		private GridSelection _selection;
		private TreePath _prev_sel_path;
		private GridViewColumn _prev_sel_col;

		construct {
			this.can_focus = true;
			set_flags (get_flags () | WidgetFlags.NO_WINDOW);

			_selection = new GridSelection (this);
			_orientation = Orientation.VERTICAL;

			this.style_set += on_style_set;
			this.state_changed += on_state_changed;
			this.focus_in_event += on_focus_in_event;
			this.focus_out_event += on_focus_out_event;
			this.button_press_event += on_button_press_event;
			this.key_press_event += on_key_press_event;
			_selection.changed += on_selection_changed;

			cda = new DrawingArea ();
			cda.expose_event += on_cda_expose_event;
			cda.add_events (EventMask.POINTER_MOTION_MASK);
			cda.motion_notify_event += on_yheader_motion_notify_event;
			cda.add_events (EventMask.BUTTON_PRESS_MASK);
			cda.button_press_event += on_yheader_button_press_event;
			cda.add_events (EventMask.BUTTON_RELEASE_MASK);
			cda.button_release_event += on_yheader_button_release_event;
			cda.add_events (EventMask.SCROLL_MASK);
			add (cda);

			tda = new DrawingArea ();
			tda.expose_event += on_tda_expose_event;
			tda.add_events (EventMask.POINTER_MOTION_MASK);
			tda.motion_notify_event += on_yheader_motion_notify_event;
			tda.add_events (EventMask.BUTTON_PRESS_MASK);
			tda.button_press_event += on_yheader_button_press_event;
			tda.add_events (EventMask.BUTTON_RELEASE_MASK);
			tda.button_release_event += on_yheader_button_release_event;
			tda.add_events (EventMask.SCROLL_MASK);

			lda = new DrawingArea ();
			lda.expose_event += on_lda_expose_event;
			lda.add_events (EventMask.SCROLL_MASK);
			lda.button_press_event += on_lda_button_press_event;

			fvp = new Viewport (null, null);
			fvp.shadow_type = ShadowType.NONE;

			fda = new DrawingArea ();
			fda.style_set += on_fda_style_set;
			fda.realize += on_fda_realized;
			fda.expose_event += on_fda_expose_event;
			fda.add_events (EventMask.BUTTON_PRESS_MASK);
			fda.button_press_event += on_fda_button_press_event;
			fda.add_events (EventMask.SCROLL_MASK);
			fvp.add (fda);
			add (fvp);

			tvp = new Viewport (fvp.hadjustment, null);
			tvp.shadow_type = ShadowType.NONE;
			tvp.add (tda);
			add (tvp);

			lvp = new Viewport (null, fvp.vadjustment);
			lvp.shadow_type = ShadowType.NONE;
			lvp.add (lda);
			add (lvp);

			hbar = new HScrollbar (fvp.hadjustment);
			var hbar_linked = new LinkedScrollbar (hbar);
			hbar_linked.link (tda);
			hbar_linked.link (fda);
			add (hbar);

			vbar = new VScrollbar (fvp.vadjustment);
			var vbar_linked = new LinkedScrollbar (vbar);
			vbar_linked.link (lda);
			vbar_linked.link (fda);
			add (vbar);
		}

		public TreeModel model {
			get { return _model; }
			set {
				if (_model == value) {
					return;
				}

				_selection.deselect ();

				_model = value;

				if (_model != null) {
					_model.row_changed += on_model_row_changed;
					_model.row_deleted += on_model_row_deleted;
					_model.row_inserted += on_model_row_inserted;
					_model.rows_reordered += on_model_rows_reordered;
				}

				rebuild_dimensions ();

				cda.queue_draw ();
				tda.queue_draw ();
				lda.queue_draw ();
				fda.queue_draw ();
			}
		}

		public GridViewColumn[] columns {
			get {
				// XXX-VALA: simple array conversion like this would be nice
//				return _columns.to_array (typeof (GridViewColumn));
				GridViewColumn[] array = new GridViewColumn[_columns.size];
				for (int i = 0; i < array.length; i++) {
					array[i] = _columns.get (i);
				}
				return array;
			}
		}

		public PolicyType hscrollbar_policy {
			get { return _hbar_policy; }
			set {
				if (_hbar_policy == value) {
					return;
				}

				_hbar_policy = value;

				queue_resize ();
			}
		}

		public PolicyType vscrollbar_policy {
			get { return _vbar_policy; }
			set {
				if (_vbar_policy == value) {
					return;
				}

				_vbar_policy = value;

				queue_resize ();
			}
		}

		public ScrollbarSpan hscrollbar_span {
			get { return _hbar_span; }
			set {
				if (_hbar_span == value) {
					return;
				}

				_hbar_span = value;

				queue_resize ();
			}
		}

		public ScrollbarSpan vscrollbar_span {
			get { return _vbar_span; }
			set {
				if (_vbar_span == value) {
					return;
				}

				_vbar_span = value;

				queue_resize ();
			}
		}

		public Orientation orientation {
			get { return _orientation; }
			set {
				if (_orientation != value) {
					_orientation = value;

					rebuild_dimensions ();

					orientation_changed ();
				}
			}
		}

		public GridSelection selection {
			get { return _selection; }
		}

		public int n_row_headers {
			get { return r_span; }
			set {
				if (value >= 0 && value <= _columns.size) {
					r_span = value;

					update_drawing_area_size_requests ();
				}
			}
		}

		public int n_col_headers {
			get { return c_span; }
			set {
				TreeIter i;
				if (value >= 0 &&
				    (value == 0 || _model.iter_nth_child (out i, null,
				                                          value - 1)))
				{
					c_span = value;

					update_drawing_area_size_requests ();
				}
			}
		}

		public void append_column (GridViewColumn col) {
			col.visibility_changed += on_column_visibility_changed;

			if (col.visible) {
				if (_orientation == Orientation.VERTICAL) {
					_widths.add (0);
				} else {
					_heights.add (0);
				}

				_visible.add (col);

				TreeIter i = TreeIter ();

				if (_model != null && _model.get_iter_first (out i)) {
					do {
						TreePath path = _model.get_path (i);
						measure_cell (path, col);
					} while (_model.iter_next (ref i));
				}

				if (this.visible) {
					update_drawing_area_size_requests ();
				}
			}

			_columns.add (col);

			columns_changed ();
		}

		public GridViewColumn append_column_with_attributes (CellRenderer r,
		                                        RenderAttribute[]? a = null)
		{
			var col = new GridViewColumn (r);
			col.set_field_attrs (a);
			append_column (col);

			return col;
		}

		public GridViewColumn append_column_with_data_func (CellRenderer r,
		                                                    GridCellDataFunc f)
		{
			return append_column_with_header (r, f, null, null);
		}

		public GridViewColumn append_column_with_header (CellRenderer r,
		                                                 GridCellDataFunc f,
		                                                 CellRenderer? hr,
		                                                 GridCellDataFunc? hf)
		{
			var col = new GridViewColumn (r);
			col.set_cell_data_func (f);
			if (hr != null) {
				col.set_header_renderer (hr);
				col.set_header_cell_data_func (hf);
			}
			append_column (col);

			return col;
		}

		public int remove_column (GridViewColumn col) {
			if (!_columns.contains (col)) {
				warning ("Trying to remove non-existent column");
				return _columns.size;
			}

			if (col.visible) {
				if (_orientation == Orientation.VERTICAL) {
					_widths.remove_at (_visible.index_of (col));
				} else {
					_heights.remove_at (_visible.index_of (col));
				}

				_visible.remove (col);
				update_drawing_area_size_requests ();
			}

			_columns.remove (col);

			columns_changed ();

			return _columns.size;
		}

		public bool get_selectable (TreePath? path, GridViewColumn? col) {
			TreeIter i;

			return col != null &&
			       _columns.contains (col) &&
			       col.visible &&
			       _visible.index_of (col) >= r_span &&
			       path != null &&
			       path.get_indices() [0] >= c_span &&
			       _model.get_iter (out i, path);
		}

		public void scroll_to_cell (TreePath path, GridViewColumn col, bool a,
		                            float ra, float ca)
		{
			Adjustment adj;
			Widget widget;
			Rectangle rect = cell_rect (path, col, out widget);

			if (widget == tda || widget == fda) {
				adj = fvp.hadjustment;

				if (a) {
					adj.value = double.min (adj.upper - adj.page_size,
					                        (rect.x + rect.width / 2) -
					                         adj.page_size * ra);
				} else {
					if (rect.x < adj.value ||
						rect.width > fvp.allocation.width)
					{
						adj.value -= adj.value - rect.x;
					} else if (Rect.right (rect) >
					           fvp.allocation.width + adj.value)
					{
						adj.value = Rect.right (rect)
						          - fvp.allocation.width;
					}
				}
			}

			if (widget == lda || widget == fda) {
				adj = fvp.vadjustment;

				if (a) {
					adj.value = double.min (adj.upper - adj.page_size,
					                        (rect.y + rect.height / 2) -
					                        adj.page_size * ca);
				} else {
					if (rect.y < adj.value ||
					    rect.height > fvp.allocation.height)
					{
						adj.value -= adj.value - rect.y;
					} else if (Rect.bottom (rect) >
							   fvp.allocation.height + adj.value)
					{
						adj.value = Rect.bottom (rect)
						          - fvp.allocation.height;
					}
				}
			}
		}

		protected override void add (Widget w) {
			w.set_parent (this);
			_children.add (w);
		}

		protected override void remove (Widget w) {
			if (w != null) {
				_children.remove (w);
				w.unparent ();
			}
		}
		

		protected override void forall (bool include_internals, Gtk.Callback cb) {
			foreach (Widget widget in _children) {
				cb (widget);
			}
		}

		protected override void size_request (out Requisition r) {
			Requisition bar_req;
			/**
			 * XXX: this doesn't change when the visibility of a scrollbar
			 *      changes.
			 **/
			vbar.size_request (out bar_req);
			r.width =
				this.left_width +
				(this.x_span < _widths.size ? _widths.get (this.x_span) : 0) +
				(int) this.border_width + bar_req.width;
			hbar.size_request (out bar_req);
			r.height =
				this.top_height +
				(this.y_span < _heights.size ? _heights.get (this.y_span) : 0) +
				(int) this.border_width + bar_req.height;
		}

		/**
		 * we need to call queue_draw () on each of the drawing areas to avoid
		 * some nasty unpainted rect artifacts on win32
		 **/
		protected override void size_allocate (Rectangle a) {
			this.allocation = (Allocation) a;

			Requisition hbar_r;
			hbar.size_request (out hbar_r);
			Requisition vbar_r;
			vbar.size_request (out vbar_r);

			/**
			 * tmp variables to store the scrollbar visibilities, as flipping
			 * the .visible property can cause infinite size_allocate () loop
			 **/
			bool hbar_v, vbar_v;

			hbar_v = _hbar_policy == PolicyType.ALWAYS ||
			         (_hbar_policy == PolicyType.AUTOMATIC &&
			          this.left_width + this.field_width +
			          this.border_width > a.width);

			vbar_v = _vbar_policy == PolicyType.ALWAYS ||
			         (_vbar_policy == PolicyType.AUTOMATIC &&
			          this.top_height + this.field_height + this.border_width +
			          (hbar_v ? hbar_r.height : 0) > a.height);

			if (!vbar_v) {
				vbar_r = Requisition ();
			} else {
				/**
				 * the horizontal space used to show the vbar may now
				 * cause the hbar to be needed
				 **/
				hbar_v = _hbar_policy == PolicyType.ALWAYS ||
				         (_hbar_policy == PolicyType.AUTOMATIC &&
				          this.left_width + this.field_width +
				          this.border_width + vbar_r.width > a.width);
			}

			if (!hbar_v) {
				hbar_r = Requisition ();
			}

			hbar.visible = hbar_v;
			vbar.visible = vbar_v;

			Rectangle wa = Rectangle ();

			wa.x = a.x;
			wa.y = a.y;
			wa.width = this.left_width;
			wa.height = this.top_height;
			cda.size_allocate (wa);
			cda.queue_draw ();

			wa.x = Rect.right ((Rectangle) cda.allocation);
			wa.y = a.y;
			wa.width = a.width - this.left_width - (int) this.border_width -
			                    (_vbar_span != ScrollbarSpan.HEADER_OVERLAP ?
			                     vbar_r.width : 0);
			wa.height = this.top_height;
			tvp.size_allocate (wa);
			tvp.queue_draw ();

			wa.x = a.x;
			wa.y = Rect.bottom ((Rectangle) cda.allocation);
			wa.width = this.left_width;
			wa.height = a.height - this.top_height - (int) this.border_width -
			                    (_hbar_span != ScrollbarSpan.HEADER_OVERLAP ?
			                     hbar_r.height : 0);
			lvp.size_allocate (wa);
			lvp.queue_draw ();

			wa.x = a.x + this.left_width;
			wa.y = a.y + this.top_height;
			wa.width = a.width - this.left_width - (int) this.border_width -
			           vbar_r.width;
			wa.height = a.height - this.top_height - (int) this.border_width -
			           hbar_r.height;
			fvp.size_allocate (wa);
			fvp.queue_draw ();

			int gap;

			if (hbar.visible) {
				gap = _hbar_span == ScrollbarSpan.FULL ? 0 : this.left_width;
				wa.x = a.x + gap;
				wa.y = Rect.bottom ((Rectangle) a) - hbar_r.height;
				wa.width = a.width - gap - vbar_r.width;
				wa.height = hbar_r.height;
				hbar.size_allocate (wa);
			}

			if (vbar.visible) {
				gap = _vbar_span == ScrollbarSpan.FULL ? 0 : this.top_height;
				wa.x = Rect.right ((Rectangle) a) - vbar_r.width;
				wa.y = a.y + gap;
				wa.width = vbar_r.width;
				wa.height = a.height - gap - hbar_r.height;
				vbar.size_allocate (wa);
			}
		}

		private int x_span {
			get {
				return _orientation == Orientation.VERTICAL ? r_span : c_span;
			}
		}

		private int y_span {
			get {
				return _orientation == Orientation.VERTICAL ? c_span : r_span;
			}
		}

		private int left_width {
			get {
				int w = 0;

				for (int x = 0; x < this.x_span && x < _widths.size; x++) {
					w += _widths.get (x);
				}

				return w;
			}
		}

		private int top_height {
			get {
				int h = 0;

				for (int y = 0; y < this.y_span && y < _heights.size; y++) {
					h += _heights.get (y);
				}

				return h;
			}
		}

		private int field_width {
			get {
				int w = 0;

				for (int x = this.x_span; x < _widths.size; x++) {
					w += _widths.get (x);
				}

				return w;
			}
		}

		private int field_height {
			get {
				int h = 0;

				for (int y = this.y_span; y < _heights.size; y++) {
					h += _heights.get (y);
				}

				return h;
			}
		}

		private void on_model_row_changed (TreeModel m, TreePath path,
		                                   TreeIter iter)
		{
			foreach (GridViewColumn col in _visible) {
				// We don't just use the path directly,
				// as for some bloody reason the path.depth
				// gets corrupted on win32 randomly resulting 
				// in depths of 10923461, and things go boom.
				TreePath p = _model.get_path (iter);
				measure_cell (p, col);
				invalidate_cell_rect (p, col);
			}

			update_drawing_area_size_requests ();
		}

		private void on_model_row_deleted (TreeModel m, TreePath path) {
			if (_orientation == Orientation.VERTICAL) {
				_heights.remove_at (path.get_indices () [0]);
			} else {
				_widths.remove_at (path.get_indices () [0]);
			}

			update_drawing_area_size_requests ();
		}

		private void on_model_row_inserted (TreeModel m, TreePath path) {
			if (_orientation == Orientation.VERTICAL) {
				_heights.insert (path.get_indices () [0], 0);
			} else {
				_widths.insert (path.get_indices () [0], 0);
			}
		}

		private void on_model_rows_reordered (TreeModel m) {
			// XXX
		}

		private void on_style_set (GridView s, Style? style) {
			rebuild_dimensions ();
		}

		private void on_state_changed (GridView s, StateType previous_state) {
			Rectangle ir = Rectangle ();

			ir.x = 0; ir.y = 0;
			ir.width = cda.allocation.width; ir.height = cda.allocation.height;
			cda.window.invalidate_rect (ir, true);

			ir.x = 0; ir.y = 0;
			ir.width = tda.allocation.width; ir.height = tda.allocation.height;
			tda.window.invalidate_rect (ir, true);

			ir.x = 0; ir.y = 0;
			ir.width = lda.allocation.width; ir.height = lda.allocation.height;
			lda.window.invalidate_rect (ir, true);

			ir.x = 0; ir.y = 0;
			ir.width = fda.allocation.width; ir.height = fda.allocation.height;
			fda.window.invalidate_rect (ir, true);
		}

		private bool on_focus_in_event (GridView s, EventFocus event) {
			TreePath spath;
			GridViewColumn scol;

			if (_selection.get_selected (out spath, out scol)) {
				invalidate_cell_rect (spath, scol);
			}

			return false;
		}

		private bool on_focus_out_event (GridView s, EventFocus event) {
			TreePath spath;
			GridViewColumn scol;

			if (_selection.get_selected (out spath, out scol)) {
				invalidate_cell_rect (spath, scol);
			}

			return false;
		}

		private bool on_button_press_event (GridView s, EventButton event) {
			grab_focus ();
			return false;
		}

		private bool on_key_press_event (GridView s, EventKey event) {
			TreePath spath;
			GridViewColumn scol;

			if (!_selection.get_selected (out spath, out scol)) {
				return false;
			}

			int x, y;

			translate_coords (spath, scol, out x, out y);

			switch (event.keyval) {
				case Gdk.KEY_UP:
					y -= 1;
					break;
				case Gdk.KEY_DOWN:
					y += 1;
					break;
				case Gdk.KEY_LEFT:
					x -= 1;
					break;
				case Gdk.KEY_RIGHT:
					x += 1;
					break;
				default:
					return false;
			}

			if (translate_coords_b (x, y, out spath, out scol) &&
			    _selection.select_cell (spath, scol))
			{
				scroll_to_cell (spath, scol, false, 0, 0);
			}

			return true;
		}

		private void on_selection_changed (GridSelection s) {
			if (_model != null && get_selectable (_prev_sel_path,
			                                      _prev_sel_col))
			{
				invalidate_cell_rect (_prev_sel_path, _prev_sel_col);
			}

			TreePath spath;
			GridViewColumn scol;

			if (_selection.get_selected (out spath, out scol)) {
				invalidate_cell_rect (spath, scol);
			}

			if (spath != null) {
				_prev_sel_path = spath.copy ();
			} else {
				_prev_sel_path = null;
			}
			_prev_sel_col = scol;
		}

		private bool on_cda_expose_event (DrawingArea da, EventExpose event) {
			if (_model == null) {
				return false;
			}

			GridViewColumn col;
			TreePath path;

			for (int x = 0; x < this.x_span; x++) {
				for (int y = 0; y < this.y_span; y++) {
					if (translate_coords_b (x, y, out path, out col)) {
						draw_cell (path, col, event.area);
					}
				}
			}

			return false;
		}

		private bool on_tda_expose_event (DrawingArea da, EventExpose event) {
			if (_model == null) {
				return false;
			}

			GridViewColumn col;
			TreePath path;

			for (int x = this.x_span; x < _widths.size; x++) {
				for (int y = 0; y < this.y_span; y++) {
					if (translate_coords_b (x, y, out path, out col)) {
						draw_cell (path, col, event.area);
					}
				}
			}

			return false;
		}

		private bool on_lda_expose_event (DrawingArea da, EventExpose event) {
			if (_model == null) {
				return false;
			}

			GridViewColumn col;
			TreePath path;

			for (int x = 0; x < this.x_span; x++) {
				for (int y = this.y_span; y < _heights.size; y++) {
					if (translate_coords_b (x, y, out path, out col)) {
						draw_cell (path, col, event.area);
					}
				}
			}

			return false;
		}

		private bool on_fda_expose_event (DrawingArea da, EventExpose event) {
			if (_model == null) {
				return false;
			}

			GridViewColumn col;
			TreePath path;

			for (int x = this.x_span; x < _widths.size; x++) {
				for (int y = this.y_span; y < _heights.size; y++) {
					if (translate_coords_b (x, y, out path, out col)) {
						draw_cell (path, col, event.area);
					}
				}
			}

			return false;
		}

		private bool on_yheader_motion_notify_event (DrawingArea da,
		                                             EventMotion event)
		{
			int x = (int) event.x;

			if (da == tda) {
				x += left_width;
			}

			da.window.set_cursor (
				_drag_col >= 0 || drag_x (x) >= 0 ? DRAG_CURSOR : null);

			if (_drag_col >= 0 && x > column_x (_drag_col) + 12) {
				int prev_width = _widths.get (_drag_col);
				_widths.set (_drag_col, x - column_x (_drag_col));

				/**
				 * make sure that we don't cause the grid's size request
				 * to increase
				 **/
				Requisition vbar_req;
				vbar.size_request (out vbar_req);
				if (_drag_col <= this.x_span &&
				    column_x (this.x_span) + _widths.get (this.x_span) +
				    (this.x_span + 1 < _widths.size ?
				    _widths.get (this.x_span + 1) : 0) +
				    this.border_width +
				    (vbar.visible ? vbar_req.width : 0) + 6 >=
				    allocation.width)
				{
					_widths.set (_drag_col, prev_width);
					return false;
				}

				update_drawing_area_size_requests ();
			}

			return false;
		}

		private bool on_yheader_button_press_event (DrawingArea da,
		                                            EventButton event)
		{
			int x = (int) event.x;

			if (da == tda) {
				x += this.left_width;
			}

			_drag_col = drag_x (x);
			
			// make the headers eat clicks, so that when you
			// override button_press in classes using GridView, you
			// don't get spurious clicks.
			return true;
		}

		private bool on_yheader_button_release_event (DrawingArea da,
		                                              EventButton event)
		{
			_drag_col = -1;
			return false;
		}

		private void on_fda_style_set (DrawingArea da, Style? style) {
			if ((fda.get_flags () & WidgetFlags.REALIZED) != 0) {
				fda.window.set_background (fda.style.@base[(int) fda.state]);
			}
		}

		private void on_fda_realized (DrawingArea da) {
			fda.window.set_background (fda.style.@base[(int) fda.state]);
		}

		private bool on_fda_button_press_event (DrawingArea da,
		                                        EventButton event)
		{
			if (event.y > this.field_height) {
				return false;
			}

			int x = this.x_span, y = this.y_span;

			while (x < _widths.size &&
			       (int) event.x > column_x (x) - this.left_width)
			{
				x++;
			}
			while ((int) event.y > row_y (y) - this.top_height) {
				y++;
			}

			GridViewColumn col, scol;
			TreePath path, spath;

			translate_coords_b (x - 1, y - 1, out path, out col);
			_selection.get_selected (out spath, out scol);

			if (spath != null && path.compare (spath) == 0 && col == scol &&
			    (event.state & ModifierType.CONTROL_MASK)
			                         == ModifierType.CONTROL_MASK)
			{
				_selection.deselect ();
			} else {
				_selection.select_cell (path, col);
				scroll_to_cell (path, col, false, 0, 0);
			}

			return false;
		}

		// make the headers eat clicks, so that when you override
		// button_press in classes using GridView, you don't get
		// spurious clicks.
		private bool on_lda_button_press_event (DrawingArea da,
		                                        EventButton event)
		{
			return true;
		}

		private void on_column_visibility_changed (GridViewColumn changed_col) {
			int vc;
			Gee.List<int> dimensions = _orientation == Orientation.VERTICAL ?
			                           _widths : _heights;

			if (changed_col.visible) {
				_visible.clear ();

				foreach (GridViewColumn col in _columns) {
					if (col.visible) {
						_visible.add (col);
					}
				}

				vc = _visible.index_of (changed_col);
				dimensions.insert (vc, 0);

				TreeIter i = TreeIter ();

				if (_model != null && _model.get_iter_first (out i)) {
					do {
						TreePath path = _model.get_path (i);
						measure_cell (path, changed_col);
					} while (_model.iter_next (ref i));
				}
			} else {
				vc = _visible.index_of (changed_col);

				if (dimensions.size > vc) { 
					dimensions.remove_at (vc);
				}

				_visible.remove (changed_col);
			}

			update_drawing_area_size_requests ();
		}

		private void translate_coords (TreePath path, GridViewColumn col,
		                               out int x, out int y)
		{
			if (_orientation == Orientation.VERTICAL) {
				x = _visible.index_of (col);
				y = path.get_indices () [0];
			} else {
				x = path.get_indices () [0];
				y = _visible.index_of (col);
			}
		}

		// XXX-VALA: find a better name
		private bool translate_coords_b (int x, int y,
		                                 out TreePath path,
		                                 out GridViewColumn col)
		{
			int c = _orientation == Orientation.VERTICAL ? x : y;
			int r = _orientation == Orientation.VERTICAL ? y : x;
			TreeIter i;

			if (c >= _visible.size || !_model.iter_nth_child (out i, null, r)) {
				col = null;
				path = null;

				return false;
			} else {
				col = _visible.get (c);
				path = new TreePath.from_string (r.to_string ());

				return true;
			}
		}

		private void update_drawing_area_size_requests () {
			cda.set_size_request (this.left_width, this.top_height);
			tda.set_size_request (this.field_width, this.top_height);
			lda.set_size_request (this.left_width, this.field_height);
			fda.set_size_request (this.field_width, this.field_height);
		}

		private void measure_cell (TreePath path, GridViewColumn col) {
			int w, h, x, y, x_offset, y_offset;
			TreeIter i;
			bool header = path.get_indices () [0] < c_span;
			Rectangle rect = Rectangle ();
			CellRenderer renderer = header && col.header_renderer != null ?
			                        col.header_renderer : col.field_renderer;

			_model.get_iter (out i, path);
			col.cell_set_cell_data (_model, i, header);
			// XXX-VALA: vapi change: parameter 'rect' should be ref
			renderer.get_size (this, rect, out x_offset, out y_offset,
			                   out w, out h);
			translate_coords (path, col, out x, out y);

			_widths.set (x, int.max (_widths.get (x), w + 20));
			_heights.set (y, int.max (_heights.get (y), h + 4));
		}

		private void rebuild_dimensions () {
			_widths.clear ();
			_heights.clear ();

			int x, y;
			TreeIter i = TreeIter ();
			TreePath path;

			if (_model != null && _model.get_iter_first (out i)) {
				do {
					foreach (GridViewColumn col in _visible) {
						path = _model.get_path (i);
						translate_coords (path, col, out x, out y);

						if (x == _widths.size) {
							_widths.add (0);
						}
						if (y == _heights.size) {
							_heights.add (0);
						}

						measure_cell (path, col);
					}
				} while (_model.iter_next (ref i));
			}

			update_drawing_area_size_requests ();
		}

		private Rectangle cell_rect (TreePath path, GridViewColumn col,
		                             out Widget widget)
		{
			int x, y;
			translate_coords (path, col, out x, out y);

			Rectangle rect = Rectangle ();
			rect.x = column_x (x);
			rect.y = row_y (y);
			rect.width = _widths.get (x);
			rect.height = _heights.get (y);

			if (x < this.x_span && y < this.y_span) {
				widget = cda;
				return rect;
			} else if (x < this.x_span) {
				widget = lda;
				rect.y -= this.top_height;
			} else if (y < this.y_span) {
				widget = tda;
				rect.x -= this.left_width;
			} else {
				widget = fda;
				rect.x -= this.left_width;
				rect.y -= this.top_height;
			}

			/* stretch the rightmost cell to fill the allocation */
			if (x == _widths.size - 1) {
				rect.width = Rect.right ((Rectangle) fda.allocation) - rect.x;
			}

			return rect;
		}

		private void invalidate_cell_rect (TreePath path, GridViewColumn col) {
			Widget widget;
			Rectangle rect = cell_rect (path, col, out widget);

			if (widget != null && widget.window != null) {
				widget.window.invalidate_rect (rect, true);
			}
		}

		private int column_x (int x) {
			int ret = 0;
			for (--x; x >= 0 && x < _widths.size; x--) {
				ret += _widths.get (x);
			}

			return ret;
		}

		private int row_y (int y) {
			int ret = 0;
			for (--y; y >= 0 && y < _heights.size; y--) {
				ret += _heights.get (y);
			}

			return ret;
		}

		private int drag_x (int x) {
			for (int xc = 1; xc < _widths.size; xc++) {
				int distance = x - column_x (xc);

				if (distance > -3 && distance < 3 &&
				    (xc != this.x_span || distance < 0))
				{
					return xc - 1;
				}
			}

			return -1;
		}

		private void draw_cell (TreePath path, GridViewColumn col,
		                        Rectangle clip_)
		{
			Widget w;
			Rectangle clip = Rectangle ();

			Rectangle rect = cell_rect (path, col, out w);
			// XXX-VALA: attention: parameter 'clip' is an out parameter!
			if (!clip_.intersect (rect, clip)) {
				return;
			}

			CellRendererState crs = 0;

			if (w != fda) {
				w.window.draw_rectangle (w.style.mid_gc [w.state], true,
				                         rect.x, rect.y,
				                         rect.width, rect.height);
			} else {
				TreePath spath;
				GridViewColumn scol;
				_selection.get_selected (out spath, out scol);

				if (spath != null && path.compare (spath) == 0 && col == scol) {
					crs = CellRendererState.SELECTED;
					Gtk.paint_flat_box (w.style, w.window,
					                    StateType.SELECTED, ShadowType.NONE,
					                    clip, this, "cell_odd", rect.x, rect.y,
					                    rect.width, rect.height);
				}
			}

			Gdk.draw_line (w.window, w.style.bg_gc [w.state],
			               rect.x, Rect.bottom (rect) - 1,
			               Rect.right (rect), Rect.bottom (rect) - 1);
			Gdk.draw_line (w.window, w.style.bg_gc [w.state],
			               Rect.right (rect) - 1, rect.y,
			               Rect.right (rect) - 1, Rect.bottom (rect));

			TreeIter i;
			bool header = path.get_indices () [0] < c_span;
			CellRenderer renderer = header && col.header_renderer != null ?
			                        col.header_renderer : col.field_renderer;
			_model.get_iter (out i, path);
			col.cell_set_cell_data (_model, i, header);
			renderer.render (w.window, this, rect, rect, clip, crs);
		}
	}
}


// XXX-VALA: these are additional helper definitions for the Vala port

namespace Geetk {

	public struct RenderAttribute {

		public string property;
		public int column;

		public RenderAttribute (string p, int c) {
			this.property = p;
			this.column = c;
		}
	}
}

namespace Array {

	public int index_of (GLib.Object[] array, GLib.Object item) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == item) {
				return i;
			}
		}
		return -1;
	}
}

namespace Gdk {
	public const uint KEY_LEFT = 0xFF51;
	public const uint KEY_RIGHT = 0xFF53;
	public const uint KEY_UP = 0xFF52;
	public const uint KEY_DOWN = 0xFF54;
}

namespace Gdk.Rect {

	public static int right (Rectangle rect) {
		return rect.x + rect.width;
	}

	public static int bottom (Rectangle rect) {
		return rect.y + rect.height;
	}

	public static int top (Rectangle rect) {
		return rect.y;
	}

	public static int left (Rectangle rect) {
		return rect.x;
	}
}

