/* 
 * Medsphere.Widgets
 * Copyright (C) 2007 Medsphere Systems Corporation
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/**
 * CollapsePaned
 *
 * CPaned is a helper class for vpaneds with an _expander child
 **/

/**
 * XXX: _snap_lock is not being correctly set when the paned is moved using the
 *      keyboard.
 * XXX: this code only works when the_expander is paned.Child2. it could be made
 *      to work for either Child1/Child2 with some added logic to decide whether
 *      to add/subtract, use min/max, etc.
 **/

using Gtk;
using Gdk;

namespace Geetk {

	public class CPaned : GLib.Object {

		public Paned paned { get; construct; }

		private Expander _expander;

		private bool _size_alloc_lock = false;
		private bool _snap_lock = false;

		protected double stored_pos { private get; private set; default = 0.5; }

		public CPaned (Paned paned) {
			this.paned = paned;
		}

		public CPaned.with_position (Paned paned, double initial_pos) {
			this.paned = paned;
			this.stored_pos = initial_pos;
		}

		construct {
			if (!(this.paned is VPaned) || !(this.paned.child2 is Expander)) {
				warning ("CPaned: this ain't gonna fly");
//				return;
			}

			_expander = this.paned.child2 as Expander;

			paned.size_allocate += on_paned_size_allocated;
			paned.accept_position += on_paned_accept_position;
			paned.button_press_event += on_paned_button_press_event;
			paned.button_release_event += on_paned_button_release_event;

			paned.notify += (s, p) => {
				switch (p.name) {
				case "position":
					on_paned_position_changed (s);
					break;
				case "expanded":
					on_expanded_changed (s);
					break;
				}
			};
		}

		private void on_paned_size_allocated (Paned p, Rectangle a) {
			if (_expander.expanded) {
				_stored_pos =
					(double) paned.position / (double) paned.allocation.height;
			}

			if (_size_alloc_lock) {
				_size_alloc_lock = false;;
				return;
			}

			snap ();
		}

		private bool on_paned_accept_position (Paned p) {
			snap ();
			return false;
		}

//		[ConnectBefore]
		private bool on_paned_button_press_event (Paned p, EventButton e) {
			_snap_lock = true;
			return false;
		}

//		[ConnectBefore]
		private bool on_paned_button_release_event (Paned p, EventButton e) {
			_snap_lock = false;
			snap ();
			return false;
		}

		private void on_paned_position_changed (Paned p) {
			if (!_snap_lock) {
				return;
			}

			_size_alloc_lock = true;

			Requisition req;
			_expander.child.size_request (out req);
			bool e = paned.position + req.height <
			         paned.max_position + (_expander.expanded ? req.height : 0);

			if (_expander.expanded != e) {
				_expander.expanded = e;
			}

			if (e) {
				this.stored_pos =
					(double) paned.position / (double) paned.allocation.height;
			}
		}

		private void on_expanded_changed (Paned p) {
			if (_expander.expanded) {
				_size_alloc_lock = true;
				paned.position = (int) (paned.allocation.height * this.stored_pos);
			} else {
				Requisition e_req;
				_expander.size_request (out e_req);
				Requisition ec_req;
				_expander.child.size_request (out ec_req);

				int height = (e_req.height - ec_req.height);

				_expander.set_size_request (-1, (height >= -1) ? height : -1);
				snap ();
				_expander.set_size_request (-1, -1);
			}
		}

		/**
		 * OH SNAP
		 * http://baz.medsphere.com/~brad/Pelican%20Oh%20snap.JPG
		 **/
		private void snap () {
			if (_expander.expanded || _snap_lock) {
				return;
			}

			_size_alloc_lock = true;
			this.paned.position = this.paned.allocation.height;
		}
	}
}

