namespace Geetk {

	public class SampleTile : Tile {

		public string text { private get; construct; }
		private Pango.Layout _text_layout;

		public SampleTile (string text) {
			this.text = text;
		}

		construct {
			_text_layout = create_pango_layout (text);
		}

		public override Tile copy () {
			return new SampleTile (this.text);
		}

		public override void draw_content (Cairo.Context context, Gdk.Rectangle area) {
			context.set_source_rgb (0, 0, 0);
			Pango.cairo_update_layout (context, _text_layout);
			context.move_to (area.x, area.y);
			Pango.cairo_show_layout (context, _text_layout);
		}
	}
}
