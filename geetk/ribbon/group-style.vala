namespace Geetk {

	/**
	 * Position of a widget in a group of widget.
	 */
	public enum GroupStyle {
		ALONE,
		LEFT,
		CENTER,
		RIGHT
	}
}

