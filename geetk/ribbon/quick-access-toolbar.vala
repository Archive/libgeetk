using GLib;
using Gtk;
using Gee;

namespace Geetk {

	public class QuickAccessToolbar : Container {

		private Gee.List<Widget> _widgets;
		private int[] _widths;

		construct {
			set_flags (get_flags () | WidgetFlags.NO_WINDOW);

			add_events (Gdk.EventMask.BUTTON_PRESS_MASK
						| Gdk.EventMask.BUTTON_RELEASE_MASK
						| Gdk.EventMask.POINTER_MOTION_MASK);

			_widgets = new Gee.ArrayList<Widget> ();
		}

		/**
		 * Adds a widget before all existing widgets.
		 *
		 * @param widget    The widget to add.
		 */
		public void prepend (Widget widget) {
			insert (widget, 0);
		}

		/**
		 * Adds a widget after all existing widgets.
		 *
		 * @param widget    The widget to add.
		 */
		public void append (Widget widget) {
			insert (widget, -1);
		}

		/**
		 * Inserts a widget at the specified location.</summary>
		 *
		 * @param widget   The widget to add.
		 * @param index    The index (starting at 0) at which the widget must
		 *                 be inserted, or -1 to insert the widget after all
		 *                 existing widgets.
		 */
		public void insert (Widget widget, int index) {
			widget.set_parent (this);
			widget.visible = true;

			if (index == -1) {
				_widgets.add (widget);
			} else {
				_widgets.insert (index, widget);
			}

			show_all ();
		}

		/**
		 * Removes the widget at the specified index.
		 *
		 * @param index   Index of the widget to remove.
		 */
		public void remove (int index) {
			if (index == -1) {
				index = _widgets.size -1;
			}

			_widgets.get (index).parent = null;
			_widgets.remove_at (index);

			show_all ();
		}

		protected override void forall (bool include_internals, Gtk.Callback callback) {
			foreach (var widget in _widgets) {
				if (widget.visible) {
					callback (widget);
				}
			}
		}

		protected override void size_request (out Requisition requisition) {
			base.size_request (out requisition);

			if (_widths == null || _widths.length != _widgets.size) {
				_widths = new int[_widgets.size];
			}

			requisition.height = 16;
			requisition.width = 0;

			int i = 0;
			foreach (var button in _widgets) {
				button.height_request = requisition.height;
				Requisition req;
				button.size_request (out req);
				requisition.width += req.width;
				_widths[i++] = req.width;
			}
			if (this.height_request != -1) {
				requisition.height = this.height_request;
			}
			if (this.width_request != -1) {
				requisition.width = this.width_request;
			}
		}

		protected override void size_allocate (Gdk.Rectangle allocation) {
			base.size_allocate (allocation);

			int i = 0;
			int x = allocation.x;
			foreach (var button in _widgets) {
				var r = Gdk.Rectangle () {
					x = x,
					y = allocation.y,
					width = _widths[i],
					height = allocation.height
				};
				button.size_allocate (r);
				x += r.width;
				i++;
			}
		}
	}
}

