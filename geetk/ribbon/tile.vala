using Gtk;

namespace Geetk {

	/**
	 * The Tile widget.
	 */
	public abstract class Tile : Widget {

		/** Gets or sets the width of the border. */
		private uint _border_width;
		public uint border_width {
			set {
				_border_width = value;
				queue_draw ();
			}
			get { return _border_width; }
		}

		/** Gets or sets the state of the Tile. */
		private bool _selected;
		public bool selected {
			set {
				_selected = value;
				queue_draw ();
			}
			get { return _selected; }
		}

		/** Fired when the Tile has been clicked. */
		public signal void clicked ();

		/** Theme used to draw the widget. */
		private Theme _theme = new Theme ();
		public Theme theme {
			set {
				_theme = value;
				queue_draw ();
			}
			get { return _theme; }
		}

		/** Construction method */
		construct {
			set_flags (get_flags () | WidgetFlags.NO_WINDOW);
			
			add_events (Gdk.EventMask.BUTTON_PRESS_MASK
					| Gdk.EventMask.BUTTON_RELEASE_MASK
					| Gdk.EventMask.POINTER_MOTION_MASK);
				
			this.selected = false;
			this.border_width = 4;
		}

		/** Creates a carbon copy of the current Tile. */
		public abstract Tile copy ();

		protected override bool expose_event (Gdk.EventExpose evnt) {
			var cr = Gdk.cairo_create (this.window);

			var area = Gdk.Rectangle () {
				x = evnt.area.x,
				y = evnt.area.y,
				width = evnt.area.width,
				height = evnt.area.height
			};
			var alloc = Gdk.Rectangle () {
				x = this.allocation.x,
				y = this.allocation.y,
				width = this.allocation.width,
				height = this.allocation.height
			};
			var content_area = Gdk.Rectangle () {
				x = alloc.x + (int) this.border_width,
				y = alloc.y + (int) this.border_width,
				width = alloc.width - 2 * (int) this.border_width,
				height = alloc.height - 2 * (int) this.border_width
			};

			cr.rectangle (area.x, area.y, area.width, area.height);
			cr.clip ();
			_theme.draw_tile (cr, alloc, content_area, this);
			
			draw_content (cr, content_area);
			
//			cr.target.dispose ();
//			cr.dispose ();
			
			return base.expose_event (evnt);
		}

		/**
		 * Draws the content of the tile.
		 *
		 * @param context  Cairo context to be used to draw the content.
		 * @param area     Area that can be painted.
		 */
		public abstract void draw_content (Cairo.Context context, Gdk.Rectangle area);

		// TODO

		protected override bool button_press_event (Gdk.EventButton evnt) {
//			bool ret = base.button_press_event (evnt);
			bool ret = false;
			
			queue_draw ();
			return ret;
		}
		
		protected override bool button_release_event (Gdk.EventButton evnt) {
//			bool ret = base.button_release_event (evnt);
			bool ret = false;
			clicked ();
			queue_draw ();
			return ret;
		}
		
		protected override bool enter_notify_event (Gdk.EventCrossing evnt) {
//			bool ret = base.enter_notify_event (evnt);
			bool ret = false;
			
			queue_draw ();
			return ret;
		}
		
		protected override bool leave_notify_event (Gdk.EventCrossing evnt) {
//			bool ret = base.leave_notify_event (evnt);
			bool ret = false;
			
			queue_draw ();
			return ret;
		}
	}
}

