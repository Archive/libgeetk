using Gtk;

namespace Geetk {

	/** EventBox extended to transmit all events. */
	public class ExtraEventBox : EventBox {

		protected override bool event (Gdk.Event event) {
			if (event.any.window == this.window) {
				if (event.type != Gdk.EventType.EXPOSE) {
					if (this.child != null) {
						this.child.event (event);
					}
				}
			}
//			return base.event (event);
			return false;
		}
	}
}
