namespace Gee {

	public void list_remove_range (List list, int index, int count) {
		for (int i = 0; i < count; i++) {
			list.remove_at (index);
		}
	}
}

namespace Cairo {

	public static void set_color (Context cr, Color c) {
		cr.set_source_rgba (c.r, c.g, c.b, c.a);
	}

	public static void add_stop (Pattern p, double offset, Color c) {
		p.add_color_stop_rgba (offset, c.r, c.g, c.b, c.a);
	}
}

namespace Gdk.Rect {

	// FIXME GEETK :`Gdk.Rect' already contains a definition
//	public static int right (Gdk.Rectangle rect) {
//		return rect.x + rect.width;
//	}

//	public static int bottom (Gdk.Rectangle rect) {
//		return rect.y + rect.height;
//	}

//	public static int top (Gdk.Rectangle rect) {
//		return rect.y;
//	}

//	public static int left (Gdk.Rectangle rect) {
//		return rect.x;
//	}

	public static bool contains (Gdk.Rectangle rect, int x, int y) {
		return ((x >= left (rect)) && (x <= right (rect)) && 
				(y >= top (rect)) && (y <= bottom (rect)));
	}

	public static void inflate (Rectangle rect, int width, int height) {
		rect.x -= width;
		rect.y -= height;
		rect.width += width * 2;
		rect.height += height * 2;
	}
}

namespace Gdk {

	public Gdk.Color new_color (uint8 r, uint8 g, uint8 b) {
		var color = Gdk.Color () {
			red = (ushort) (r << 8 | r),
			green = (ushort) (g << 8 | g),
			blue = (ushort) (b << 8 | b),
			pixel = 0
		};
		return color;
	}

	public struct Size { 
		public int width;
		public int height;

		public Size (int width, int height) {
			this.width = width;
			this.height = height;
		}
	}
}

public struct Color {

	public double r;
	public double g;
	public double b;
	public double a;

	public Color.from_rgb (double r, double g, double b) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = 1.0;
	}

	public Color.from_rgba (double r, double g, double b, double a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
}

