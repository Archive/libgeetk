#include <gtk/gtk.h>
#include <geetk/gridview/gridview.h>

static const char* DEMO_DATA = "OpenVista is a cost-effective, open, trusted "
	"and complete EHR which enhances patient safety, increases clinical and "
	"operational efficiency and provides an opportunity to improve quality of "
	"care delivery. Healthcare provider organizations are encouraged to access "
	"this software and see first hand how our commercialized version of the "
	"Veterans Affairs' FOIA VistA can be leveraged outside of the VA to "
	"support the goal of higher quality healthcare. We anticipate that an open "
	"source approach will assist in revolutionizing healthcare by enabling "
	"many more healthcare delivery organizations to successfully adopt such "
	"technology for the benefit of their patients and their businesses. "
	"Medsphere is focused on developing an open source community centered on "
	"Medsphere OpenVista®. We welcome your participation in OpenVista "
	"projects. If you have an idea on how to improve quality, enhance "
	"functionality, or accelerate innovation in OpenVista we encourage and "
	"welcome your input -- please join our mailing lists.";

static const gint n_columns = 5;

GeetkGridView* grid_view = NULL;

static void on_orientation_combo_box_changed (GtkComboBox* s)
{
	g_return_if_fail (s == NULL || GTK_IS_COMBO_BOX (s));

	geetk_grid_view_set_orientation (grid_view,
	                                 (gtk_combo_box_get_active (s) == 0)
	                                   ? GTK_ORIENTATION_VERTICAL
	                                   : GTK_ORIENTATION_HORIZONTAL);
}


static void on_row_headers_spin_button_value_changed (GtkSpinButton* s)
{
	g_return_if_fail (s == NULL || GTK_IS_SPIN_BUTTON (s));

	geetk_grid_view_set_n_row_headers (grid_view,
	                                   gtk_spin_button_get_value_as_int (s));
}


static void on_col_headers_spin_button_value_changed (GtkSpinButton* s)
{
	g_return_if_fail (s == NULL || GTK_IS_SPIN_BUTTON (s));

	geetk_grid_view_set_n_col_headers (grid_view,
	                                   gtk_spin_button_get_value_as_int (s));
}


int main (int argc, char ** argv)
{
	GtkWidget* win;
	GtkWidget* vbox;
	GtkWidget* hbox;
	GtkComboBox* orientation_combo_box;
	GtkSpinButton* row_headers_spin_button;
	GtkSpinButton* col_headers_spin_button;
	GtkListStore* store;
	GtkTreeIter tree_iter;
	GType* type_array;
	char** words;
	int i;

	gtk_init (&argc, &argv);

	win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_resize (GTK_WINDOW (win), 640, 480);
	g_signal_connect (GTK_OBJECT (win), "destroy",
	                  G_CALLBACK (gtk_main_quit), NULL);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (win), GTK_WIDGET (vbox));

	type_array = g_new (GType, n_columns);
	for (i = 0; i < n_columns; i++) {
		type_array[i] = G_TYPE_STRING;
	}

	words = g_strsplit (DEMO_DATA, " ", 0);
	store = gtk_list_store_newv (n_columns, type_array);
	gtk_list_store_append (store, &tree_iter);

	for (i = 0; words[i] != NULL; i++) {
		GValue val = {0};
		int col = i % n_columns;
		g_value_init (&val, G_TYPE_STRING);
		g_value_set_string (&val, words[i]);
		gtk_list_store_set_value (store, &tree_iter, col, &val);
		if (col == n_columns - 1) {
			gtk_list_store_append (store, &tree_iter);
		}
	}

	grid_view = geetk_grid_view_new ();
	geetk_grid_view_set_hscrollbar_policy (grid_view, GTK_POLICY_AUTOMATIC);
	geetk_grid_view_set_vscrollbar_policy (grid_view, GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET (grid_view), TRUE, TRUE, 0);

	for (i = 0; i < n_columns; i++) {
		GtkCellRenderer* renderer = gtk_cell_renderer_text_new ();
		GeetkRenderAttribute attrs[] = { {"text", i} };
		geetk_grid_view_append_column_with_attributes (grid_view, renderer,
		                                               attrs, 1);
	}

	geetk_grid_view_set_model (grid_view, GTK_TREE_MODEL (store));
	gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET (gtk_hseparator_new ()),
	                    FALSE, FALSE, 0);
	hbox = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox),
	                    GTK_WIDGET (gtk_label_new ("Orientation:")),
	                    FALSE, FALSE, 0);

	orientation_combo_box = (GtkComboBox*) gtk_combo_box_new_text ();
	gtk_box_pack_start (GTK_BOX (hbox), GTK_WIDGET (orientation_combo_box),
	                    FALSE, FALSE, 0);
	gtk_combo_box_append_text (orientation_combo_box, "Vertical");
	gtk_combo_box_append_text (orientation_combo_box, "Horizontal");
	gtk_combo_box_set_active (orientation_combo_box, 0);

	g_signal_connect (orientation_combo_box, "changed",
	                  G_CALLBACK (on_orientation_combo_box_changed), NULL);

	gtk_box_pack_start (GTK_BOX (hbox),
	                    GTK_WIDGET (gtk_label_new ("Row Headers:")),
	                    FALSE, FALSE, 0);

	row_headers_spin_button = (GtkSpinButton*)
	           gtk_spin_button_new_with_range (0.0, (double) n_columns, 1.0);

	gtk_spin_button_set_value (row_headers_spin_button,
	                   (double) geetk_grid_view_get_n_row_headers (grid_view));

	g_signal_connect (GTK_EDITABLE (row_headers_spin_button), "changed",
	                  G_CALLBACK (on_row_headers_spin_button_value_changed),
	                  NULL);

	gtk_box_pack_start (GTK_BOX (hbox), GTK_WIDGET (row_headers_spin_button),
	                    FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox),
	                    GTK_WIDGET (gtk_label_new ("Col Headers:")),
	                    FALSE, FALSE, 0);

	col_headers_spin_button = (GtkSpinButton*) gtk_spin_button_new_with_range (
	    0.0,
	    (double) gtk_tree_model_iter_n_children (GTK_TREE_MODEL (store), NULL),
	    1.0);

	gtk_spin_button_set_value (col_headers_spin_button,
	                   (double) geetk_grid_view_get_n_col_headers (grid_view));

	g_signal_connect (GTK_EDITABLE (col_headers_spin_button), "changed",
	                  G_CALLBACK (on_col_headers_spin_button_value_changed),
	                  NULL);

	gtk_box_pack_start (GTK_BOX (hbox), GTK_WIDGET (col_headers_spin_button),
	                    FALSE, FALSE, 0);

	gtk_widget_show_all (win);

	gtk_main ();

	return 0;
}

