using GLib;
using Cairo;
using Gtk;
using Geetk;

public class Sample : SyntheticWindow {

	protected bool _compose_available = false;

	private ToolBox _flow_0;
	private Menu _main_menu;

	construct {
		this.title = "Ribbons Sample";

		add_events (Gdk.EventMask.BUTTON_PRESS_MASK
				| Gdk.EventMask.BUTTON_RELEASE_MASK
				| Gdk.EventMask.POINTER_MOTION_MASK);

		var master = new VBox (false, 0);

		master.add_events (Gdk.EventMask.BUTTON_PRESS_MASK
				| Gdk.EventMask.BUTTON_RELEASE_MASK
				| Gdk.EventMask.POINTER_MOTION_MASK);

		var button_0 = new Geetk.Button.with_label ("Hello World");
		var group_0 = new RibbonGroup ();
		group_0.label = "Summer of Code";
		group_0.add (button_0);
		group_0.expanded += on_click;

		var open_menu = new Menu ();
		var abc_txt = new MenuItem.with_label ("abc.txt");
		open_menu.append (abc_txt);
		var foo_txt = new MenuItem.with_label ("foo.txt");
		open_menu.append (foo_txt);

		var open = Geetk.Button.from_stock_with_label (STOCK_OPEN, "Open", false);
		open.drop_down_menu = open_menu;
		open.clicked += on_click;

		var button_1 = new Geetk.Button.with_label ("Menu Test");
		button_1.clicked += on_click;
		var button_1_menu = new Menu ();
		var option_1 = new MenuItem.with_label ("Option 1");
		button_1_menu.append (option_1);
		button_1.drop_down_menu = button_1_menu;

		var file_tool_pack = new ToolPack ();
		file_tool_pack.append_button (Geetk.Button.from_stock_with_label (STOCK_NEW, "New", false));
		file_tool_pack.append_button (open);
		file_tool_pack.append_button (Geetk.Button.from_stock_with_label (STOCK_SAVE, "Save", false));

		var printer_tool_pack = new ToolPack ();
		printer_tool_pack.append_button (Geetk.Button.from_stock_with_label (STOCK_PRINT, "Print", false));

		var font_tool_pack = new ToolPack ();
		font_tool_pack.append_button (Geetk.ToggleButton.from_stock (STOCK_BOLD, false));
		font_tool_pack.append_button (Geetk.ToggleButton.from_stock (STOCK_ITALIC, false));
		font_tool_pack.append_button (Geetk.ToggleButton.from_stock (STOCK_UNDERLINE, false));

		var font_combo = Gtk.combo_box_from_entries (new string[] {"Arial", "Verdana"});
		font_combo.active = 0;

		// var flow_0 = new FlowLayoutContainer ();
		_flow_0 = new ToolBox ();
		_flow_0.append (file_tool_pack);
		_flow_0.append (printer_tool_pack);
		_flow_0.append (font_tool_pack);
		_flow_0.append (font_combo);

		var btn_flow_box = new HBox (false, 2);
		btn_flow_box.add (button_1);
		btn_flow_box.add (_flow_0);

		// Little hack because Gtk+ is not designed to support size negociations
		btn_flow_box.size_allocate += (sender, allocation) => {
			// XXX: flow_0 must be a field, since Vala doesn't support real closures yet.
			_flow_0.height_request = allocation.height;
		};

		var group_1 = new RibbonGroup ();
		group_1.label = "I will be back";
		group_1.add (btn_flow_box);

		var gallery = new Gallery ();
		gallery.append_tile (new SampleTile ("1"));
		gallery.append_tile (new SampleTile ("2"));
		gallery.append_tile (new SampleTile ("3"));
		gallery.append_tile (new SampleTile ("4"));
		gallery.append_tile (new SampleTile ("5"));

		var group_2 = new RibbonGroup ();
		group_2.label = "Gallery";
		group_2.add (gallery);

		var page_0 = new HBox (false, 2);
		page_0.pack_start (group_0, false, false, 0);
		page_0.pack_start (group_1, false, false, 0);
		page_0.pack_start (group_2, false, false, 0);

		var page_1 = new HBox (false, 2);
		var group_1_0 = new RibbonGroup ();
		group_1_0.label = "Welcome on the second page";
		page_1.pack_start (group_1_0, false, false, 0);

		var page_2 = new HBox (false, 2);

		var page_label_0 = new Label ("Page 1");
		var page_label_1 = new Label ("Page 2");
		var page_label_2 = new Label ("Page 3");

		var shortcuts = new Geetk.Button.with_label ("Menu");
		shortcuts.child.modify_fg (StateType.NORMAL, Gdk.new_color (255, 255, 255));

		_main_menu = new Menu ();
		var main_menu_quit = new MenuItem.with_label ("Quit");
		main_menu_quit.activate += () => {
			Gtk.main_quit ();
		};
		_main_menu.append (main_menu_quit);

		shortcuts.clicked += () => {
			// XXX: main_menu must be a field, since Vala doesn't support real closures yet.
			_main_menu.popup (null, null, null, 3, Gtk.get_current_event_time ());		// FIXME: sane default parameters
			_main_menu.show_all ();
		};

		var qat = new QuickAccessToolbar ();
		qat.append (Geetk.Button.from_stock (STOCK_NEW, false));
		qat.append (Geetk.Button.from_stock (STOCK_SAVE, false));

		var ribbon = new Ribbon ();
		ribbon.application_button = new ApplicationButton ();
		ribbon.quick_access_toolbar = qat;
//		ribbon.shortcuts = shortcuts;
		ribbon.append_page (page_0, page_label_0);
		ribbon.append_page (page_1, page_label_1);
		ribbon.append_page (page_2, page_label_2);

		var txt = new TextView ();
			
		master.pack_start (ribbon, false, false, 0);
		master.pack_start (txt, true, true, 0);

		add (master);

//		this.screen_changed += on_screen_changed;
//		screen_changed (null);
//		this.expose_event += on_expose_event;
		this.destroy += Gtk.main_quit;

		resize (200, 200);
	}

	private void on_click () {
		var d = new Dialog.with_buttons ("Test", this, DialogFlags.DESTROY_WITH_PARENT);
		d.modal = true;
		d.add_button ("Close", ResponseType.CLOSE);
		d.run ();
		d.destroy ();
	}

//	[ConnectBefore]		// FIXME: does that work?
//	private bool on_expose_event (Sample sender, Gdk.EventExpose event) {
//		var cr = Gdk.cairo_create (this.window);

//		if (_compose_available) {
//			cr.set_source_rgba (0, 0, 0, 0.3);
//		} else {
//			cr.set_source_rgb (0.3, 0.3, 0.3);
//		}

////		cr.set_source_rgb (0.749, 0.859, 1.0);

//		cr.set_operator (Operator.SOURCE);
//		cr.paint ();

//		cr.target.dispose ();
//		cr.dispose ();

//		return false;
//	}

//	private void on_screen_changed (Sample sender, Gdk.Screen? previous_screen) {
//		weak Gdk.Colormap cm = this.screen.get_rgba_colormap ();
//		_compose_available = cm != null;	// FIX: Does not seem to detect compose support in all cases 

//		if (!_compose_available) {
//			cm = this.screen.get_rgb_colormap ();
//		}
//		set_colormap (cm);

//		print ("Compose support: %s\n", _compose_available ? "true" : "false");
//	}

	public static int main (string[] args) {
		Gtk.init (ref args);

		var sample = new Sample ();
		sample.show_all ();

		Gtk.main ();

		return 0;
	}
}

namespace Gtk {

	public static ComboBox combo_box_from_entries (string[] entries) {
		var combo = new ComboBox.with_model (new ListStore (1, typeof (string)));
		var cell = new CellRendererText ();
		combo.pack_start (cell, true);
		combo.set_attributes (cell, "text", 0);
		foreach (var entry in entries) {
			combo.append_text (entry);
		}
		return combo;
	}
}

