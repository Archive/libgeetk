using Gtk;
using Geetk;

namespace DemoData {

	public static const string TEXT = "OpenVista is a cost-effective, open, trusted and complete EHR which enhances patient safety, increases clinical and operational efficiency and provides an opportunity to improve quality of care delivery. Healthcare provider organizations are encouraged to access this software and see first hand how our commercialized version of the Veterans Affairs' FOIA VistA can be leveraged outside of the VA to support the goal of higher quality healthcare. We anticipate that an open source approach will assist in revolutionizing healthcare by enabling many more healthcare delivery organizations to successfully adopt such technology for the benefit of their patients and their businesses. Medsphere is focused on developing an open source community centered on Medsphere OpenVista®. We welcome your participation in OpenVista projects. If you have an idea on how to improve quality, enhance functionality, or accelerate innovation in OpenVista we encourage and welcome your input -- please join our mailing lists.";
}

public class DemoWindow : Window {

	public DemoWindow (string title) {
		this.title = title;
	}

	construct {
		this.destroy += Gtk.main_quit;
		resize (640, 480);
	}
}

public class CPanedDemo : DemoWindow {

	private const int n_columns = 5;

	private TreeView tree_view;

	private Expander expander;
	private TextView text_view;

	private CPaned cpaned;

	public CPanedDemo () {
		base ("CPaned Demo");
	}

	construct {
		var vpaned = new VPaned ();
		add (vpaned);

		var top_scrolled_window = new ScrolledWindow (null, null);
		vpaned.pack1 (top_scrolled_window, true, false);

		GLib.Type[] type_array = new GLib.Type[n_columns];
		for (int x = 0; x < n_columns; x++) {
			type_array[x] = typeof (string);
		}

		string[] words = DemoData.TEXT.split (" ");
		var store = new ListStore.newv (n_columns, type_array);
		TreeIter tree_iter;
		store.append (out tree_iter);

		for (int x = 0; words[x] != null; x++) {
			int col = x % n_columns;
			Value val = Value (typeof (string));
			val.set_string (words[x]);
			store.set_value (tree_iter, col, val);

			if (col == n_columns - 1) {
				store.append (out tree_iter);
			}
		}

		tree_view = new TreeView ();
		top_scrolled_window.add (tree_view);

		for (int x = 0; x < n_columns; x++) {
			tree_view.append_column (new TreeViewColumn.with_attributes (
			                               "Column " + x.to_string (),
			                               new CellRendererText (), "text", x));
		}

		tree_view.model = store;

		expander = new Expander ("Details");
		vpaned.pack2 (expander, true, false);

		var align = new Alignment (0.0f, 0.0f, 1.0f, 1.0f);
		align.left_padding = 12;
		expander.add (align);

		var bottom_scrolled_window = new ScrolledWindow (null, null);
		bottom_scrolled_window.shadow_type = ShadowType.ETCHED_IN;
		align.add (bottom_scrolled_window);

		text_view = new TextView ();
		text_view.editable = false;
		bottom_scrolled_window.add (text_view);

		tree_view.get_selection ().changed += on_tree_view_selection_changed;

		cpaned = new CPaned (vpaned);
	}

	private void on_tree_view_selection_changed () {
		TreeIter tree_iter;
		TextBuffer buffer = text_view.buffer;

		string t = "1\n2\n3\n4\n5\n6\n7\n";
		buffer.set_text (t, (int) t.size ());

//		if (tree_view.get_selection ().get_selected (out tree_iter)) {
//			buffer.clear ();
//			TextIter text_iter = buffer.start_iter;

//			for (int x = 0; x < n_columns; x++) {
//				string value =
//					tree_view.model.get_value (tree_iter, x) as string;
//				buffer.insert (ref text_iter,
//				               "%d: %s\n".printf (x, value));
//			}

//			expander.expanded = true;
//		} else {
//			buffer.text = "";
//			expander.expanded = false;
//		}
	}
}

static int main (string[] args) {
	Gtk.init (ref args);

	var demo = new CPanedDemo ();

	demo.show_all ();
	Gtk.main ();

	return 0;
}

